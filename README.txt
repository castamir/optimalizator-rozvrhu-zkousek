////////////////////////////////////////////////////////////
INFORMACE
////////////////////////////////////////////////////////////
Autor: Miroslav Paul�k
Pr�ce: Optimaliz�tor rozvrhu zkou�ek na FIT
Typ: Diplomov� pr�ce
Rok: 2014/2015

////////////////////////////////////////////////////////////
OBSAH
////////////////////////////////////////////////////////////
    1. Popis aplikace
    2. Po�adavky
    3. Spu�t�n� aplikace
    4. Licence

////////////////////////////////////////////////////////////
1. Popis aplikace
////////////////////////////////////////////////////////////
Aplikace generuje rozvrh zkou�ek na z�klad� omezen�, kter�
jsou obsa�eny v souboru "dpdata.pl". V�stupem je seznam
zkou�ek s p�i�azen�m dnem a hodinou kon�n�. V p��pad�
spu�t�n� skriptn� verze jsou nav�c p�i�azeny i konkr�tn�
u�ebny. P��klady vstupn�ch dat jsou uvedeny ve slo�ce
src/input/.

D�le�it�m nastaven�m ve vstupn�m souboru je rozsah hodnoty
penaliza�n� funkce (velikosti dom�ny penaliza�n� prom�nn�).
Tuto hodnotu lze zam�nit v souboru vstupu �pravou
n�sleduj�c�ho ��dku:

	set_penale_limits(0,0),
	
nap�. pro omezen� penalizace na interval 5-15 je nutn�
nahradit tento ��dek n�sledovn�:

	set_penale_limits(5,15),
	
Av�ak nen� doporu�eno zad�vat interval hodnot s rozsahem
v�t��m, ne� 1.
	
////////////////////////////////////////////////////////////
2. Po�adavky
////////////////////////////////////////////////////////////
SWI Prolog v6.6


////////////////////////////////////////////////////////////
3. Spu�t�n� aplikace
////////////////////////////////////////////////////////////
Aplikaci je mo�no spou�t�t dvoj�m zp�sobem:

1) p�ekladem do bin�rn� podoby pomoc� p�ilo�en�ho n�stroje
Makefile (toto je doporu�en� postup):

make

a n�sledn� spu�t�n� p��kazem

./thesis

2) spu�t�n�m p��mo v prost�ed� SWI Prolog
swipl
?- consult(thesis).
?- once(play).

////////////////////////////////////////////////////////////
4. Licence
////////////////////////////////////////////////////////////
Copyright (c) 2015 Miroslav Paul�k

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the 
Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, 
sublicense, and/or sell copies of the Software, and to 
permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
